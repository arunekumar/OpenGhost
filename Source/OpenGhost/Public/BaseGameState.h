// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "BaseGameMode.h"
#include "BaseGameState.generated.h"

enum class ETeamID : uint8;
struct FGameModeConfig;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FPlayerAddedDelegate, ABasePlayerState*, PlayerState);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FPlayerRemovedDelegate, ABasePlayerState*, PlayerState);

UCLASS()
class OPENGHOST_API ABaseGameState : public AGameState
{
	GENERATED_BODY()
	
public:
	ABaseGameState();

	virtual float GetPlayerRespawnDelay(class AController* Controller) const override;

	void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const;
	void UpdateTeamScore(ETeamID TeamID);
	void AddTeamRound(ETeamID TeamID);

	//BP helpers
	UFUNCTION(BlueprintCallable)
	bool IsTeamBased() const;

	UFUNCTION(BlueprintCallable)
	bool HasValidConfig() const;

	UFUNCTION(BlueprintCallable)
	EMatchState GetDetailedMatchState() const;

	UFUNCTION(BlueprintCallable)
	void SetDetailedMatchState(EMatchState NewState);

private:

	virtual void AddPlayerState(APlayerState* PlayerState) override;
	virtual void RemovePlayerState(APlayerState* PlayerState) override;

public:
	UPROPERTY(BlueprintReadOnly, Transient, Replicated)
	TArray<int32> myTeamScores;

	UPROPERTY(BlueprintReadOnly, Transient, Replicated)
	TArray<int32> myTeamRounds;

	UPROPERTY(BlueprintReadOnly, Transient, Replicated)
	int32 myRemainingTime;

	UPROPERTY(BlueprintReadOnly, Transient, Replicated)
	bool myIsTimerPaused;

	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly, Replicated)
	FGameModeConfig myModeConfig;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = GameMode)
	FPlayerAddedDelegate myOnPlayerAdded;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = GameMode)
	FPlayerRemovedDelegate myOnPlayerRemoved;

	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly, Replicated)
	EMatchState myDetailedMatchState;

	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly, Replicated)
	ABasePlayerState* myRoundWinner;

	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly, Replicated)
	ABasePlayerState* myMatchWinner;
};
