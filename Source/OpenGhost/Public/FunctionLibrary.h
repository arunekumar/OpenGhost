// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CommonTypes.h"
#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "FunctionLibrary.generated.h"

class ABaseGameMode;

UCLASS()
class OPENGHOST_API UFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category = Library)
	static ABaseGameMode* GetGameMode(const AActor* WorldRef);

	UFUNCTION(BlueprintCallable, Category = Library)
	static EWinCondition GetWinCondition(const AActor* WorldRef);

	UFUNCTION(BlueprintCallable, Category = Library)
	static FTimespan GetCurrentTime();

	UFUNCTION(BlueprintCallable, Category = Library)
	static FDateTime GetCurrentDateAndTime();
};
