// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "CommonTypes.h"
#include "GameModeConfig.generated.h"

USTRUCT(Blueprintable)
struct OPENGHOST_API FGameModeConfig
{
	GENERATED_BODY()

	FGameModeConfig();
	void EnsureReplication();

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = GameMode)
	bool myIsTeamBased;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = GameMode)
	EWinCondition myWinCondition;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = GameMode)
	int32 myWinThreshold;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = GameMode)
	bool myEnableCheats;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = GameMode)
	bool myDirtyFlag;

	UPROPERTY(VisibleInstanceOnly, Category = GameMode)
	uint32 myReplicationBit;
};

USTRUCT(Blueprintable)
struct OPENGHOST_API FServerGameModeConfig : public FGameModeConfig
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = GameMode)
	bool myAllowRespawn;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = GameMode)
	bool myAllowSelfDamage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = GameMode)
	bool myFriendlyFire;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = GameMode)
	float mySelfDamageModifier;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = GameMode)
	float myKillScore;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = GameMode)
	float myDeathScore;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = GameMode)
	float mySuicideScore;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = GameMode)
	float myAssistScore;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = GameMode)
	int32 myRoundPreparationTime;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = GameMode)
	int32 myRoundTime;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = GameMode)
	int32 myTimeBetweenRounds;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = GameMode)
	int32 myMatchRestartTime;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = GameMode)
	ETeamID myDefaultWinTeam;
};