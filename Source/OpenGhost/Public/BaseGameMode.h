// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CommonTypes.h"
#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "GameModeConfig.h"
#include "BaseGameMode.generated.h"

class ABaseGameState;
class ABasePlayerController;
class ABasePlayerState;
class APlayerStart;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FPlayerJoinedDelegate, ABasePlayerState*, PlayerState);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FPlayerLeftDelegate, ABasePlayerState*, PlayerState);

UCLASS()
class OPENGHOST_API ABaseGameMode : public AGameMode
{
	GENERATED_BODY()
	
public:

	ABaseGameMode();

	UFUNCTION(BlueprintCallable)
	bool CanDamage(const ABasePlayerState* Player, const ABasePlayerState* OtherPlayer) const;

	virtual float ModifyDamage(float Damage, AActor* DamagedActor, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) const;
	virtual void Killed(AController* Killer, AController* KilledPlayer, APawn* KilledPawn, const UDamageType* DamageType, AController* Assist = nullptr);
	virtual void Suicide(AController* KilledPlayer, APawn* KilledPawn, const UDamageType* DamageType);
	virtual bool IsSpawnpointAllowed(APlayerStart* SpawnPoint, AController* Player) const;
	virtual void MatchTimer();

	virtual void PreLogin(const FString& Options, const FString& Address, const FUniqueNetIdRepl& UniqueId, FString& ErrorMessage) override;
	virtual void PostLogin(APlayerController* NewPlayer) override;
	virtual void Logout(AController* ExitingPlayer) override;
	virtual void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) override;
	virtual bool AllowCheats(APlayerController* P) override;
	virtual AActor* ChoosePlayerStart_Implementation(AController* Player) override;
	virtual void RestartGame() override;
	virtual void PreInitializeComponents() override;
	virtual void PostInitializeComponents() override;
	virtual void HandleMatchIsWaitingToStart() override;
	virtual void HandleMatchHasStarted() override;
	virtual void HandleMatchHasEnded() override;
	virtual void StartMatch() override;
	virtual void StartPlay() override;
	virtual void Reset() override;
	virtual void StartToLeaveMap() override;

	// Reset overrides
	bool ShouldReset(ABaseGameState* GameState) const { return false; };
	bool ShouldReset(ABasePlayerState* PlayerState) const { return false; };

	UFUNCTION(exec)
	void FinishMatch();
	void RequestFinishAndExitToMainMenu();
	EMatchState GetDetailedMatchState() const;

	UFUNCTION(exec)
	void FinishRound();
	void StartRound();
	void RestartRound();
	void PrepareRound();
	bool DoesAllowRespawn() const;
	virtual void OnPlayerScoreChanged(ABasePlayerState* Player, int32 Score);
	virtual void OnTeamScoreChanged(ETeamID Team, int32 Score);
	virtual void OnTeamRoundsChanged(ETeamID Team, int32 Rounds);
	virtual void OnAlivePlayerCountChanged();
	virtual void SetDetailedMatchState(EMatchState DetailedState, bool CalledByBasicState = false);

	UFUNCTION(BlueprintCallable, Category = GameMode)
	TArray<ABasePlayerState*> GetWinningPlayers();

	static EMatchState ToDetailedMatchState(FName BasicState);
	static bool IsBasicMatchState(EMatchState DetailedState);
	static bool IsBasicMatchState(FName StateName);
	static FName GetBasicMatchState(EMatchState DetailedState);

	void SendMessage(FMessage Message);
	
protected:
	bool ReadyToStartMatch();
	bool ReadyToEndMatch();
	bool CanContinueMatch();
	int32 GetMinimumRequiredPlayers();

	virtual void DetermineMatchWinner();
	virtual void DetermineRoundWinner();

	// TODO define winning playerstates, save in myWinningPlayers and check for resetting the array
	virtual bool IsWinner(ABasePlayerState* PlayerState) const;
	virtual void GetWinningTeamByPoints();
	virtual void GetWinningPlayerByPoints();
	virtual void GetWinningTeamByRounds();

private:

	TArray<APlayerStart*> GetPlayerStarts(ETeamID TeamFilter = ETeamID::None);

	// Assigns a player to a team, if this hasn't been done already
	void AutoAssignTeam(ABasePlayerState* Player);
	void SetWinningTeam(ETeamID TeamID);
	void SetWinningPlayer(ABasePlayerState& Player);
	void EndCurrentDetailedMatchState();
	int32 GetTimeChunkFromState(EMatchState DetailedState);
	ETeamID IsATeamDead();
	bool CanReceiveMessage(ABasePlayerController* Player, FMessage Message) const;

	// Avoid using this. We have DetailedMatchState for a reason.
	virtual void SetMatchState(FName NewState) override;
	
public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = GameMode)
	FServerGameModeConfig myModeConfig;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = GameMode)
	FPlayerJoinedDelegate myOnPlayerJoined;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = GameMode)
	FPlayerLeftDelegate myOnPlayerLeft;

protected:

	TArray<ABasePlayerState*> myWinningPlayers;

private:

	EMatchState myDetailedMatchState;
	FTimerHandle myMatchTimerHandle;
};
