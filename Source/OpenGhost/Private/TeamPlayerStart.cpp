// Fill out your copyright notice in the Description page of Project Settings.

#include "TeamPlayerStart.h"
#include "Components/CapsuleComponent.h"

ATeamPlayerStart::ATeamPlayerStart(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
	, myAllowedTeam(ETeamID::None)
	, myIsOccupied(false)
	, myIsDirtyFlag(true)
	, myOccupationQueryTimeout(2.0f)
{
}

void ATeamPlayerStart::Reset()
{
	Super::Reset();

	myIsDirtyFlag = true;
}

bool ATeamPlayerStart::IsOccupied()
{
	if (myIsDirtyFlag)
		OccupationQuery();

	return myIsOccupied;
}

void ATeamPlayerStart::OccupationQuery()
{
	UCapsuleComponent* capsule = GetCapsuleComponent();
	FCollisionQueryParams queryParams;
	queryParams.AddIgnoredActor(this);
	queryParams.AddIgnoredComponent(capsule);
	TArray<FOverlapResult> outOverlaps;
	myIsOccupied = GetWorld()->OverlapMultiByChannel(outOverlaps, GetActorLocation(), FQuat::Identity, ECC_Pawn, FCollisionShape::MakeSphere(capsule->GetScaledCapsuleRadius()), queryParams);
	myIsDirtyFlag = false;

	GetWorldTimerManager().SetTimer(myQueryTimerHandle, this, &ATeamPlayerStart::InvalidateQuery, FMath::Max(0.1f, myOccupationQueryTimeout), false);
}

void ATeamPlayerStart::InvalidateQuery()
{
	myIsDirtyFlag = true;
}
