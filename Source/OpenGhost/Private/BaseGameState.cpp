// Fill out your copyright notice in the Description page of Project Settings.

#include "BaseGameState.h"
#include "GameModeConfig.h"
#include "BasePlayerState.h"

#include "Net/UnrealNetwork.h"

ABaseGameState::ABaseGameState()
	: myDetailedMatchState(EMatchState::Invalid)
{
	myTeamScores.AddZeroed(3);
	myTeamRounds.AddZeroed(3);

	myModeConfig.myDirtyFlag = true;
}

void ABaseGameState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABaseGameState, myTeamScores);
	DOREPLIFETIME(ABaseGameState, myTeamRounds);
	DOREPLIFETIME(ABaseGameState, myRemainingTime);
	DOREPLIFETIME(ABaseGameState, myIsTimerPaused);
	DOREPLIFETIME(ABaseGameState, myModeConfig);
	DOREPLIFETIME(ABaseGameState, myDetailedMatchState);
	DOREPLIFETIME(ABaseGameState, myRoundWinner);
	DOREPLIFETIME(ABaseGameState, myMatchWinner);
}

void ABaseGameState::UpdateTeamScore(ETeamID TeamID)
{
	int32 teamIndex = CommonTypes::TeamToInt(TeamID);
	if(!myTeamScores.IsValidIndex(teamIndex))
	{
		myTeamScores.EmplaceAt(teamIndex, 0);
	}

	int32 newScore = 0;
	for (APlayerState* player : PlayerArray)
	{
		if (ABasePlayerState* basePlayer = Cast<ABasePlayerState>(player))
		{
			if (basePlayer->GetTeam() == TeamID)
				newScore += FMath::Max(0, static_cast<int32>(basePlayer->Score));
		}
	}

	myTeamScores[teamIndex] = newScore;

	ABaseGameMode* gameMode = Cast<ABaseGameMode>(GetWorld()->GetAuthGameMode());
	if (gameMode)
		gameMode->OnTeamScoreChanged(TeamID, newScore);
}

void ABaseGameState::AddTeamRound(ETeamID TeamID)
{
	int32 teamIndex = CommonTypes::TeamToInt(TeamID);
	if (!myTeamRounds.IsValidIndex(teamIndex))
	{
		myTeamRounds.EmplaceAt(teamIndex, 0);
	}

	int32 newScore = myTeamRounds[teamIndex] + 1;
	myTeamRounds[teamIndex] = newScore;

	ABaseGameMode* gameMode = Cast<ABaseGameMode>(GetWorld()->GetAuthGameMode());
	if (gameMode)
		gameMode->OnTeamRoundsChanged(TeamID, newScore);
}

bool ABaseGameState::IsTeamBased() const
{ 
	return HasValidConfig() && myModeConfig.myIsTeamBased;
}

bool ABaseGameState::HasValidConfig() const
{
	return !myModeConfig.myDirtyFlag;
}

EMatchState ABaseGameState::GetDetailedMatchState() const
{
	return myDetailedMatchState;
}

void ABaseGameState::SetDetailedMatchState(EMatchState NewState)
{
	myDetailedMatchState = NewState;
}

void ABaseGameState::AddPlayerState(APlayerState * PlayerState)
{
	Super::AddPlayerState(PlayerState);

	if (ABasePlayerState* addedPlayer = Cast<ABasePlayerState>(PlayerState))
		myOnPlayerAdded.Broadcast(addedPlayer);
}

void ABaseGameState::RemovePlayerState(APlayerState * PlayerState)
{
	Super::RemovePlayerState(PlayerState);

	if (ABasePlayerState* removedPlayer = Cast<ABasePlayerState>(PlayerState))
		myOnPlayerRemoved.Broadcast(removedPlayer);
}

float ABaseGameState::GetPlayerRespawnDelay(class AController* Controller) const
{
	ABaseGameMode* gameMode = Cast<ABaseGameMode>(GetWorld()->GetAuthGameMode());
	return gameMode ? gameMode->MinRespawnDelay : 1.0f;	//TODO could be modified for various reasons
}