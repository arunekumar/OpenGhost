#include "CommonTypes.h"

bool CommonTypes::IsAssigned(ETeamID TeamID)
{
	return TeamID != ETeamID::Invalid;
}

bool CommonTypes::IsParticipating(ETeamID TeamID)
{
	return IsAssigned(TeamID) && TeamID != ETeamID::Spectator;
}

bool CommonTypes::BelongsToTeam(ETeamID TeamID)
{
	return TeamID == ETeamID::Team1 || TeamID == ETeamID::Team2;
}

int32 CommonTypes::TeamToInt(ETeamID TeamID)
{
	switch (TeamID)
	{
	case ETeamID::None:
		return 0;
	case ETeamID::Team1:
		return 1;
	case ETeamID::Team2:
		return 2;
	default:
		return -1;
	}
}
