// Fill out your copyright notice in the Description page of Project Settings.

#include "BasePlayerController.h"

#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "BaseCharacter.h"
#include "BaseGameMode.h"
#include "BaseGameState.h"
#include "BasePlayerState.h"
#include "Online.h"
#include "OnlineGameInstance.h"
#include "TimerManager.h"

ABasePlayerController::ABasePlayerController()
	: myAllowInputActions(false)
{
	bReplicates = true;
}

void ABasePlayerController::OnKill(ABasePlayerState* KillerPlayerState, ABasePlayerState* KilledPlayerState, const UDamageType* KillerDamageType)
{
	const auto Events = Online::GetEventsInterface();
	const auto Identity = Online::GetIdentityInterface();

	if (Events.IsValid() && Identity.IsValid())
	{
		// TODO whatever kind of online tracking you want to do
	}
}

void ABasePlayerController::OnDeath(ABasePlayerState* KillerPlayerState, ABasePlayerState* KilledPlayerState, const UDamageType* KillerDamageType, ABasePlayerState* AssistPlayerState)
{
	myOnPlayerDied.Broadcast(KillerPlayerState, KilledPlayerState, KillerDamageType, AssistPlayerState);

	const auto Events = Online::GetEventsInterface();
	const auto Identity = Online::GetIdentityInterface();

	if (Events.IsValid() && Identity.IsValid())
	{
		// TODO whatever kind of online tracking you want to do
	}
}

void ABasePlayerController::OnSuicide(ABasePlayerState* KilledPlayerState, const UDamageType * KillerDamageType)
{
	myOnPlayerSuicided.Broadcast(KilledPlayerState, KillerDamageType);

	const auto Events = Online::GetEventsInterface();
	const auto Identity = Online::GetIdentityInterface();

	if (Events.IsValid() && Identity.IsValid())
	{
		// TODO whatever kind of online tracking you want to do
	}
}

void ABasePlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	/* The following is just easier to handle in Blueprint due to UI manipulation
	InputComponent->BindAction("ChatWindow", IE_Pressed, this, &ABasePlayerController::ToggleChatWindow);
	InputComponent->BindAction("Scoreboard", IE_Pressed, this, &ABasePlayerController::OnShowScoreboard);
	InputComponent->BindAction("Scoreboard", IE_Released, this, &ABasePlayerController::OnHideScoreboard);
	InputComponent->BindAction("InGameMenu", IE_Pressed, this, &ABasePlayerController::OnToggleInGameMenu);
	*/
}

void ABasePlayerController::FailedToSpawnPawn()
{
	if (StateName == NAME_Inactive)
	{
		BeginInactiveState();
	}
	Super::FailedToSpawnPawn();
}

void ABasePlayerController::BeginInactiveState()
{
	Super::BeginInactiveState();

	// TODO switch to observerpawn

	const ABaseGameState* gameState = Cast<ABaseGameState>(GetWorld()->GetGameState());
	if(gameState && gameState->GetDetailedMatchState() == EMatchState::InRound)
		Server_RespawnRequest();
}

void ABasePlayerController::GameHasEnded(AActor * EndGameFocus, bool bIsWinner)
{
	Super::GameHasEnded(EndGameFocus, bIsWinner);

	//TODO following line should not be necessary. test if round has been successfully finished
	StopRespawn();
}

void ABasePlayerController::Reset()
{
	UnPossess();
}

FVector ABasePlayerController::GetFocalLocation() const
{
	const ABaseCharacter* character = Cast<ABaseCharacter>(GetPawn());

	// On death we want to use the player's death cam location rather than the location of where the pawn is at the moment
	// This guarantees that the clients see their death cam correctly, as their pawns have delayed destruction.
	if (character && character->IsDying())
	{
		return GetSpawnLocation();
	}

	return Super::GetFocalLocation();
}

int32 ABasePlayerController::GetGUID() const
{
	return static_cast<int32>(GetUniqueID());
}

bool ABasePlayerController::IsGameInputAllowed() const
{
	return myAllowInputActions;
}

ABaseCharacter * ABasePlayerController::GetCharacter()
{
	if (auto character = GetPawn())
		return Cast<ABaseCharacter>(character);
	return nullptr;
}

void ABasePlayerController::StopRespawn()
{
	GetWorldTimerManager().ClearTimer(myRespawnTimerHandle);
}

void ABasePlayerController::UpdateRespawnTimer()
{
	FTimerManager& timerManager = GetWorldTimerManager();
	if (timerManager.IsTimerActive(myRespawnTimerHandle))
		myRespawnTimer = timerManager.GetTimerRemaining(myRespawnTimerHandle);
}

void ABasePlayerController::UnFreeze()
{
	Super::UnFreeze();
}

void ABasePlayerController::Server_RespawnRequest_Implementation()
{
	const ABaseGameState* gameState = Cast<ABaseGameState>(GetWorld()->GetGameState());
	const ABaseGameMode* gameMode = Cast<ABaseGameMode>(GetWorld()->GetAuthGameMode());
	if (!gameState || !gameMode || !gameMode->DoesAllowRespawn())
		return;

	const float respawnDelay = gameState ? gameState->GetPlayerRespawnDelay(this) : 1.0f;
	GetWorldTimerManager().SetTimer(myRespawnTimerHandle, this, &ABasePlayerController::Respawn, respawnDelay);
}

bool ABasePlayerController::Server_RespawnRequest_Validate()
{
	return true;
}

void ABasePlayerController::Respawn()
{
	GetWorld()->GetAuthGameMode()->RestartPlayer(this);
	if (ABaseCharacter* newCharacter = Cast<ABaseCharacter>(GetPawn()))
		newCharacter->SpawnLoadout();
}

void ABasePlayerController::PreClientTravel(const FString& PendingURL, ETravelType TravelType, bool bIsSeamlessTravel)
{
	Super::PreClientTravel(PendingURL, TravelType, bIsSeamlessTravel);

	if (GetWorld())
	{
		// TODO place for loading screen shenanigans
	}
}

bool ABasePlayerController::IsMoveInputIgnored() const
{
	if (IsInState(NAME_Spectating))
	{
		// destinction between spectator states (FP, TP, free cam)
		return false;
	}
	else
	{
		return Super::IsMoveInputIgnored();
	}
}

bool ABasePlayerController::IsLookInputIgnored() const
{
	if (IsInState(NAME_Spectating))
	{
		// destinction between spectator states (FP, TP, free cam)
		return false;
	}
	else
	{
		return Super::IsLookInputIgnored();
	}
}

void ABasePlayerController::PawnPendingDestroy(APawn* P)
{
	// TODO handle camera on player death

	Super::PawnPendingDestroy(P);
}

void ABasePlayerController::Client_NotifyMatchStarted_Implementation()
{
}

void ABasePlayerController::Client_NotifyMatchEnded_Implementation()
{
}

void ABasePlayerController::Client_NotifyStartOnlineGame_Implementation()
{
	//TODO check if we need a call to the session plugin to start anything up
	// maybe TODO void ABaseGameSession::OnStartOnlineGameComplete(FName InSessionName, bool bWasSuccessful) calling this
}

void ABasePlayerController::Client_NotifyEndOnlineGame_Implementation()
{
	//TODO check if we need a call to the session plugin to clean up
	// maybe TODO void ABaseGameSession::HandleMatchHasEnded() calling this
}

void ABasePlayerController::Client_ReceiveMessage_Implementation(FMessage Message)
{
	if (UOnlineGameInstance* instance = Cast<UOnlineGameInstance>(GetGameInstance()))
	{
		instance->AddMessage(Message);
	}
}

void ABasePlayerController::Server_SendMessage_Implementation(FMessage Message)
{
	if (Message.myType == EMessageType::Server)
		return;
	ABasePlayerState* playerState = Cast<ABasePlayerState>(PlayerState);
	if (Message.myType == EMessageType::Team && playerState && CommonTypes::BelongsToTeam(playerState->GetTeam()) && Message.myTeamFilter != playerState->GetTeam())
		return;
	if (ABaseGameMode* gameMode = Cast<ABaseGameMode>(GetWorld()->GetAuthGameMode()))
		gameMode->SendMessage(Message);
}

bool ABasePlayerController::Server_SendMessage_Validate(FMessage Message)
{
	return true;
}

void ABasePlayerController::NotifyRoundEnd()
{
	StopRespawn();
	myAllowInputActions = false;
}

void ABasePlayerController::Client_NotifyRoundEnd_Implementation(bool bIsWinner, int32 ExpendedTimeInSeconds)
{
	myAllowInputActions = false;
}

void ABasePlayerController::Client_NotifyRoundStart_Implementation()
{
	myAllowInputActions = true;
}

void ABasePlayerController::HandleReturnToMainMenu()
{
	//TODO probably need a call to destroy UI here
	CleanupSessionOnReturnToMenu();
}

void ABasePlayerController::CleanupSessionOnReturnToMenu()
{
	//TODO check if we need a call to the session plugin to clean up
}

void ABasePlayerController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (!HasAuthority())
		return;

	UpdateRespawnTimer();
}

void ABasePlayerController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	//TODO maybe makes more sense in playerstate
	DOREPLIFETIME(ABasePlayerController, myRespawnTimer);
}
