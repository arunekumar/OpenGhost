#include "BaseCharacter.h"

#include "Animation/AnimInstance.h"
#include "Blueprint/UserWidget.h"
#include "Classes/Camera/CameraComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Net/UnrealNetwork.h"
#include "Runtime/UMG/Public/UMG.h"
#include "AbilityComponent.h"
#include "BaseCharacterMovementComponent.h"
#include "BaseGameMode.h"
#include "BasePlayerController.h"
#include "BasePlayerState.h"
#include "FunctionLibrary.h"
#include "SlateBasics.h"
#include "OpenGhost.h"
#include "Weapon.h"

FTakeHitInfo::FTakeHitInfo()
	: myDamage(0)
	, myDamageTypeClass(nullptr)
	, myPawnInstigator(nullptr)
	, myDamageCauser(nullptr)
	, myDamageEventClassID(0)
	, myKilled(false)
	, myEnsureReplicationByte(0)
{}

FDamageEvent& FTakeHitInfo::GetDamageEvent()
{
	switch (myDamageEventClassID)
	{
	case FPointDamageEvent::ClassID:
		if (!myPointDamageEvent.DamageTypeClass)
			myPointDamageEvent.DamageTypeClass = myDamageTypeClass ? myDamageTypeClass : UDamageType::StaticClass();
		return myPointDamageEvent;

	case FRadialDamageEvent::ClassID:
		if (!myRadialDamageEvent.DamageTypeClass)
			myRadialDamageEvent.DamageTypeClass = myDamageTypeClass ? myDamageTypeClass : UDamageType::StaticClass();
		return myRadialDamageEvent;

	default:
		if (!myGeneralDamageEvent.DamageTypeClass)
			myGeneralDamageEvent.DamageTypeClass = myDamageTypeClass ? myDamageTypeClass : UDamageType::StaticClass();
		return myGeneralDamageEvent;
	}
}

void FTakeHitInfo::SetDamageEvent(const FDamageEvent& DamageEvent)
{
	myDamageEventClassID = DamageEvent.GetTypeID();
	switch (myDamageEventClassID)
	{
	case FPointDamageEvent::ClassID:
		myPointDamageEvent = *((FPointDamageEvent const*)(&DamageEvent));
		break;
	case FRadialDamageEvent::ClassID:
		myRadialDamageEvent = *((FRadialDamageEvent const*)(&DamageEvent));
		break;
	default:
		myGeneralDamageEvent = DamageEvent;
	}

	myDamageTypeClass = DamageEvent.DamageTypeClass;
}

void FTakeHitInfo::EnsureReplication()
{
	myEnsureReplicationByte++;
}

ABaseCharacter::ABaseCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UBaseCharacterMovementComponent>(ACharacter::CharacterMovementComponentName))
	, myIngameUIWidget(nullptr)
	, myIsCrouching(false)
	, myIsAiming(false)
	, myIsWalking(false)
	, myIsSprinting(false)
	, myWantsToCrouch(false)
	, myWantsToAim(false)
	, myWantsToWalk(false)
	, myWantsToSprint(false)
	, myIsDying(false)
{
	PrimaryActorTick.bCanEverTick = true; 
	bReplicates = true;

	UCapsuleComponent* capsule = GetCapsuleComponent();
	myCameraArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraArm"));;
	myCameraArm->SetupAttachment(capsule);
	myCameraArm->bDoCollisionTest = false;

	myCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	myCamera->SetupAttachment(myCameraArm);
	myCamera->bUsePawnControlRotation = true;

	auto mesh = GetMesh();
	mesh->bOnlyOwnerSee = true;
	mesh->bOwnerNoSee = false;
	mesh->SetupAttachment(myCamera);
	mesh->bCastDynamicShadow = false;
	mesh->bReceivesDecals = false;
	mesh->VisibilityBasedAnimTickOption= EVisibilityBasedAnimTickOption::OnlyTickPoseWhenRendered;
	mesh->CastShadow = false;
	mesh->SetIsReplicated(false);
	mesh->PrimaryComponentTick.TickGroup = TG_PrePhysics;
	mesh->SetCollisionObjectType(ECC_Pawn);
	mesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	mesh->SetCollisionResponseToAllChannels(ECR_Ignore);

	myTPMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("TPMesh"));
	myTPMesh->SetIsReplicated(false);
	myTPMesh->CastShadow = true;
	myTPMesh->bCastDynamicShadow = true;
	myTPMesh->SetupAttachment(RootComponent);
	myTPMesh->bOwnerNoSee = true;
	myTPMesh->bOnlyOwnerSee = false;
	myTPMesh->bReceivesDecals = true;
	myTPMesh->SetCollisionObjectType(ECC_Pawn);
	myTPMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	myTPMesh->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Block);
	myTPMesh->SetCollisionResponseToChannel(COLLISION_PROJECTILE, ECR_Block);
	myTPMesh->SetCollisionResponseToChannel(ECC_Visibility, ECR_Block);

	capsule->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);
	capsule->SetCollisionResponseToChannel(COLLISION_PROJECTILE, ECR_Block);
	capsule->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);

	bUseControllerRotationPitch = false;
	bUseControllerRotationRoll = false;
	bUseControllerRotationYaw = true;
}

void ABaseCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (Role == ROLE_Authority)
	{
		myHealth = myMaxHealth;
	}

	UpdateMeshVisibility();
}

void ABaseCharacter::Destroyed()
{
	Super::Destroyed();
}

void ABaseCharacter::PawnClientRestart()
{
	Super::PawnClientRestart();

	UpdateMeshVisibility();
}

void ABaseCharacter::PossessedBy(class AController* Controller)
{
	Super::PossessedBy(Controller);

	if (HasAuthority())
		Client_PossessedBy(Cast<ABasePlayerController>(Controller));
}

void ABaseCharacter::OnRep_PlayerState()
{
	Super::OnRep_PlayerState();
}

void ABaseCharacter::FellOutOfWorld(const class UDamageType& DamageType)
{
	if(HasAuthority())
		Die(myHealth, nullptr, FDamageEvent(DamageType.GetClass()), nullptr);
}

void ABaseCharacter::OnReplicationPausedChanged(bool bIsReplicationPaused)
{
}

bool ABaseCharacter::IsReplicationPausedForConnection(const FNetViewer& ConnectionOwnerNetViewer)
{
	return false;
}

void ABaseCharacter::PreReplication(IRepChangedPropertyTracker & ChangedPropertyTracker)
{
	Super::PreReplication(ChangedPropertyTracker);

	DOREPLIFETIME_ACTIVE_OVERRIDE(ABaseCharacter, myLastTakenHitInfo, GetWorld() && GetWorld()->GetTimeSeconds() < myLastTakenHitTimeTimeout);
}

void ABaseCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	PlayerInputComponent->BindAxis("MoveForward", this, &ABaseCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ABaseCharacter::MoveRight);
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ABaseCharacter::Turn);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ABaseCharacter::LookUp);

	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ABaseCharacter::StartFire);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &ABaseCharacter::StopFire);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ABaseCharacter::StartJump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ABaseCharacter::StopJump);

	PlayerInputComponent->BindAction("Aim", IE_Pressed, this, &ABaseCharacter::StartAim);
	PlayerInputComponent->BindAction("AimToggle", IE_Pressed, this, &ABaseCharacter::ToggleAim);
	PlayerInputComponent->BindAction("Aim", IE_Released, this, &ABaseCharacter::StopAim);

	PlayerInputComponent->BindAction("Walk", IE_Pressed, this, &ABaseCharacter::StartWalk);
	PlayerInputComponent->BindAction("WalkToggle", IE_Pressed, this, &ABaseCharacter::ToggleWalk);
	PlayerInputComponent->BindAction("Walk", IE_Released, this, &ABaseCharacter::StopWalk);

	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &ABaseCharacter::StartCrouch);
	PlayerInputComponent->BindAction("CrouchToggle", IE_Pressed, this, &ABaseCharacter::ToggleCrouch);
	PlayerInputComponent->BindAction("Crouch", IE_Released, this, &ABaseCharacter::StopCrouch);

	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &ABaseCharacter::StartSprint);
	PlayerInputComponent->BindAction("SprintToggle", IE_Pressed, this, &ABaseCharacter::ToggleSprint);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &ABaseCharacter::StopSprint);

	PlayerInputComponent->BindAction("Reload", IE_Pressed, this, &ABaseCharacter::Reload);

	PlayerInputComponent->BindAction("Ability", IE_Pressed, this, &ABaseCharacter::ActivateAbility);
}

void ABaseCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// only owning client
	DOREPLIFETIME_CONDITION(ABaseCharacter, myWeapon, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(ABaseCharacter, myEnergy, COND_OwnerOnly);
	
	// skipping owning client
	DOREPLIFETIME_CONDITION(ABaseCharacter, myWantsToAim, COND_SkipOwner);
	DOREPLIFETIME_CONDITION(ABaseCharacter, myWantsToCrouch, COND_SkipOwner);
	DOREPLIFETIME_CONDITION(ABaseCharacter, myWantsToSprint, COND_SkipOwner);
	DOREPLIFETIME_CONDITION(ABaseCharacter, myWantsToWalk, COND_SkipOwner);
	DOREPLIFETIME_CONDITION(ABaseCharacter, myAimRotator, COND_SkipOwner);
	DOREPLIFETIME_CONDITION(ABaseCharacter, myIsCrouching, COND_SkipOwner);
	DOREPLIFETIME_CONDITION(ABaseCharacter, myIsAiming, COND_SkipOwner);
	DOREPLIFETIME_CONDITION(ABaseCharacter, myIsSprinting, COND_SkipOwner);
	DOREPLIFETIME_CONDITION(ABaseCharacter, myIsWalking, COND_SkipOwner);

	// everyone
	DOREPLIFETIME(ABaseCharacter, myHealth);

	DOREPLIFETIME_CONDITION(ABaseCharacter, myLastTakenHitInfo, COND_Custom);
}

void ABaseCharacter::BeginDestroy()
{
	Super::BeginDestroy();

}

void ABaseCharacter::Reset()
{
	Despawn();
}

void ABaseCharacter::BeginPlay()
{
	Super::BeginPlay();

	myHealth = myMaxHealth;
	myEnergy = myMaxEnergy;
}

void ABaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	UpdateMeshRotation();

	if (HasAuthority())
	{
		UpdateEnergyRegenerationCooldown(DeltaTime);
		RegenerateEnergy(DeltaTime);
	}
}

void ABaseCharacter::Die(float Damage, AController * Killer, const FDamageEvent & DamageEvent, AActor* DamageCauser)
{
	check(HasAuthority());

	if (!CanDie())
		return;

	GetCharacterMovement()->MaxWalkSpeed = 0.0f;
	myHealth = 0.0f;

	UDamageType const* const damageType = DamageEvent.DamageTypeClass ? DamageEvent.DamageTypeClass->GetDefaultObject<UDamageType>() : GetDefault<UDamageType>();
	Killer = GetDamageInstigator(Killer, *damageType);
	if (damageType->bCausedByWorld && !Killer)
		Killer = Controller;

	if (ABaseGameMode* gameMode = UFunctionLibrary::GetGameMode(this))
	{
		AController* const killedPlayer = Controller ? Controller : Cast<AController>(GetOwner());
		if (killedPlayer == Killer)
			gameMode->Suicide(killedPlayer, this, damageType);
		else
		{
			AController* const assistingPlayer = Damage >= myMaxHealth ? nullptr : GetAssistingPlayer(Killer);
			gameMode->Killed(Killer, killedPlayer, this, damageType, assistingPlayer);
		}
	}

	GetController()->UnPossess();

	NetUpdateFrequency = GetDefault<ABaseCharacter>()->NetUpdateFrequency;
	GetCharacterMovement()->ForceReplicationUpdate();

	OnDeath(Damage, DamageEvent, Killer ? Killer->GetPawn() : nullptr, DamageCauser);
}

void ABaseCharacter::OnDeath(float Damage, struct FDamageEvent const& DamageEvent, class APawn* PawnInstigator, class AActor* DamageCauser)
{
	if (myIsDying)
		return;

	if (myWeapon)
	{
		myWeapon->Detach();
		myWeapon = nullptr;
	}

	bReplicateMovement = false;
	TearOff();
	myIsDying = true;

	DestroyIngameUI();

	USkeletalMeshComponent* TPMesh = GetCharacterMesh(false);
	TPMesh->bOwnerNoSee = false;

	USkeletalMeshComponent* FPMesh = GetCharacterMesh(true);
	FPMesh->bOwnerNoSee = true;
	FPMesh->DestroyComponent();

	if (Role == ROLE_Authority)
		ReplicateHit(Damage, DamageEvent, PawnInstigator, DamageCauser, true);

	USkeletalMeshComponent* mesh = GetCharacterMesh(true);
	if (GetNetMode() != NM_DedicatedServer && myDeathSound)
		UGameplayStatics::PlaySoundAtLocation(this, myDeathSound, GetActorLocation());

	DetachFromControllerPendingDestroy();
	StopAllMontages();

	if (mesh)
	{
		static FName CollisionProfileName(TEXT("Ragdoll"));
		mesh->SetCollisionProfileName(CollisionProfileName);
	}
	SetActorEnableCollision(true);

	// TODO play death anim based on lasthit direction & location
	if (false) // get montage play duration
	{
		const float timerDuration = /*montage play duration*/ - 0.7f;
		mesh->bBlendPhysics = true;
		FTimerHandle TimerHandle;
		GetWorldTimerManager().SetTimer(TimerHandle, this, &ABaseCharacter::SetRagdollPhysics, FMath::Max(0.1f, timerDuration), false);
	}
	else
		SetRagdollPhysics();
		
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECR_Ignore);

	//OnDied.Broadcast(EventInstigator, GetController(), DamageEvent.DamageTypeClass ? DamageEvent.DamageTypeClass.GetDefaultObject() : NULL);
	//GameMode* Game = GetWorld()->GetAuthGameMode<GameMode>();
	//Game->ScoreKill(GetController(), EventInstigator, DamageEvent.DamageTypeClass);
}

void ABaseCharacter::ReplicateHit(float Damage, struct FDamageEvent const& DamageEvent, class APawn* PawnInstigator, class AActor* DamageCauser, bool bKilled)
{
	const float timeoutTime = GetWorld()->GetTimeSeconds() + 0.5f;

	FDamageEvent const& LastDamageEvent = myLastTakenHitInfo.GetDamageEvent();
	if ((PawnInstigator == myLastTakenHitInfo.myPawnInstigator.Get()) && (LastDamageEvent.DamageTypeClass == myLastTakenHitInfo.myDamageTypeClass) && (myLastTakenHitTimeTimeout == timeoutTime))
	{
		if (bKilled && myLastTakenHitInfo.myKilled)
			return;

		Damage += myLastTakenHitInfo.myDamage;
	}

	myLastTakenHitInfo.myDamage = Damage;
	myLastTakenHitInfo.myPawnInstigator = Cast<ABaseCharacter>(PawnInstigator);
	myLastTakenHitInfo.myDamageCauser = DamageCauser;
	myLastTakenHitInfo.SetDamageEvent(DamageEvent);
	myLastTakenHitInfo.myKilled = bKilled;
	myLastTakenHitInfo.EnsureReplication();

	myLastTakenHitTimeTimeout = timeoutTime;
}

void ABaseCharacter::Replicate_LastTakenHitInfo()
{
	if (myLastTakenHitInfo.myKilled)
		OnDeath(myLastTakenHitInfo.myDamage, myLastTakenHitInfo.GetDamageEvent(), myLastTakenHitInfo.myPawnInstigator.Get(), myLastTakenHitInfo.myDamageCauser.Get());
	else
		CreateHitEffects(myLastTakenHitInfo.myDamage, myLastTakenHitInfo.GetDamageEvent(), myLastTakenHitInfo.myPawnInstigator.Get(), myLastTakenHitInfo.myDamageCauser.Get());
}

void ABaseCharacter::SpawnWeapon(TSubclassOf<AWeapon> WeaponClass, FName Name)
{
	check(HasAuthority());

	FVector Location(0.0f, 0.0f, 0.0f);
	FRotator Rotation(0.0f, 0.0f, 0.0f);
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.Instigator = this;
	SpawnInfo.Owner = this;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	myWeapon = GetWorld()->SpawnActor<AWeapon>(myWeaponClass, Location, Rotation, SpawnInfo);
}

void ABaseCharacter::Server_SpawnWeapon_Implementation(TSubclassOf<AWeapon> WeaponClass, FName Name)
{
	SpawnWeapon(WeaponClass, Name);
}

bool ABaseCharacter::Server_SpawnWeapon_Validate(TSubclassOf<AWeapon> WeaponClass, FName Name)
{
	return true;
}

void ABaseCharacter::CreateIngameUI()
{
	if (myIngameUIWidget)
		return;

	APlayerController* controller = Cast<APlayerController>(GetController());
	myIngameUIWidget = CreateWidget<UUserWidget>(controller, myIngameUIWidgetClass);
	myIngameUIWidget->AddToViewport();
}

void ABaseCharacter::DestroyIngameUI()
{
	if (!myIngameUIWidget)
		return;

	myIngameUIWidget->RemoveFromViewport();
}

void ABaseCharacter::SetIngameUIVisibility(bool IsVisible)
{
	if (!myIngameUIWidget)
		return;

	if(IsVisible)
		myIngameUIWidget->SetVisibility(ESlateVisibility::Visible);
	else
		myIngameUIWidget->SetVisibility(ESlateVisibility::Hidden);
}

void ABaseCharacter::ToggleIngameUIVisibility()
{
	if (!myIngameUIWidget)
		return;

	switch (myIngameUIWidget->GetVisibility())
	{
	case ESlateVisibility::Hidden:
		myIngameUIWidget->SetVisibility(ESlateVisibility::Visible);
		break;
	default:
	case ESlateVisibility::Visible:
		myIngameUIWidget->SetVisibility(ESlateVisibility::Hidden);
		break;
	}
}

void ABaseCharacter::Despawn()
{
	if(myIngameUIWidget)
		myIngameUIWidget->RemoveFromViewport();
	if(myWeapon)
		myWeapon->Destroy();
	if(Controller)
		Controller->UnPossess();
	Destroy();
}

void ABaseCharacter::UpdateMeshRotation()
{
	if (IsLocalClientOwned())
	{
		auto rotation = GetController()->GetControlRotation();
		if (HasAuthority())
			myAimRotator = rotation;
		else
			Server_UpdateAimRotator(rotation);
		return;
	}
	if (myTPMesh->IsSimulatingPhysics())
		return;

	auto rotator = myTPMesh->GetComponentRotation();
	rotator.Pitch = 0.0f;
	myTPMesh->SetWorldRotation(rotator);
}

void ABaseCharacter::UpdateMeshVisibility()
{
	bool const isFirstPerson = IsFirstPerson();
	USkeletalMeshComponent* myMesh = GetMesh();

	myMesh->VisibilityBasedAnimTickOption = !isFirstPerson ? EVisibilityBasedAnimTickOption::OnlyTickPoseWhenRendered : EVisibilityBasedAnimTickOption::AlwaysTickPoseAndRefreshBones;
	myMesh->SetOwnerNoSee(!isFirstPerson);

	myTPMesh->VisibilityBasedAnimTickOption = isFirstPerson ? EVisibilityBasedAnimTickOption::OnlyTickPoseWhenRendered : EVisibilityBasedAnimTickOption::AlwaysTickPoseAndRefreshBones;
	myTPMesh->SetOwnerNoSee(isFirstPerson);
}

void ABaseCharacter::StopAllMontages()
{
	USkeletalMeshComponent* mesh = GetCharacterMesh(true);
	if (mesh && mesh->AnimScriptInstance)
		mesh->AnimScriptInstance->Montage_Stop(0.0f);
	mesh = GetCharacterMesh(false);
	if (mesh && mesh->AnimScriptInstance)
		mesh->AnimScriptInstance->Montage_Stop(0.0f);
}

void ABaseCharacter::Server_UpdateAimRotator_Implementation(FRotator Rotation)
{
	myAimRotator = Rotation;
}

bool ABaseCharacter::Server_UpdateAimRotator_Validate(FRotator Rotation)
{
	return true;
}

USkeletalMeshComponent* ABaseCharacter::GetDefaultCharacterMesh() const
{
	if (IsLocalClientOwned())
		return GetMesh();
	else
		return myTPMesh;
}

USkeletalMeshComponent* ABaseCharacter::GetCharacterMesh(bool GetFirstPersonMesh) const
{
	if(GetFirstPersonMesh)
		return GetMesh();
	else
		return myTPMesh;
}

void ABaseCharacter::RegenerateEnergy(float DeltaTime)
{
	if(CanRegenerateEnergy())
		myEnergy = FMath::Min(myEnergy + DeltaTime * myEnergyRegenationRate, myMaxEnergy);
}

void ABaseCharacter::UpdateEnergyRegenerationCooldown(float DeltaTime)
{
	if(myEnergyRegenerationTimer > 0.0f)
		myEnergyRegenerationTimer -= DeltaTime;
}

bool ABaseCharacter::CanRegenerateEnergy() const
{
	return !IsUsingEnergy() && myEnergyRegenerationTimer <= 0.0f;
}

void ABaseCharacter::Equip(AWeapon * Weapon)
{
	Weapon->Multicast_Equip();
}

float ABaseCharacter::TakeDamage(float Damage, const FDamageEvent& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	check(HasAuthority());

	if (myHealth <= 0.f)
		return 0.f;

	ABaseGameMode* const Game = UFunctionLibrary::GetGameMode(this);
	Damage = Game ? Game->ModifyDamage(Damage, this, DamageEvent, EventInstigator, DamageCauser) : 0.f;

	Damage = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);

	float newHealth = myHealth - Damage;
	ABasePlayerController* controller = Cast<ABasePlayerController>(EventInstigator);
	if (!myDamageDealers.Contains(controller))
		myDamageDealers.Add(controller, 0.0f);
	myDamageDealers[controller] += Damage;

	if (newHealth < 0.0f && myHealth > 0.0f)
		Die(Damage, EventInstigator, DamageEvent, DamageCauser);
	else
	{
		myHealth = newHealth;
		CreateHitEffects(Damage, DamageEvent, EventInstigator ? EventInstigator->GetPawn() : nullptr, DamageCauser);
	}

	return 0.0f;
}

bool ABaseCharacter::CanDie() const
{
	if (myIsDying
		|| IsPendingKill()
		|| Role != ROLE_Authority
		|| GetWorld()->GetAuthGameMode<AGameMode>() == nullptr)
	{
		return false;
	}

	return true;
}

bool ABaseCharacter::IsDying() const
{
	return myIsDying;
}

void ABaseCharacter::SpawnLoadout()
{
	if (!HasAuthority())
		return;
	SpawnWeapon(myWeaponClass, TEXT("MainWeapon"));

	EquipDefault();
}

void ABaseCharacter::EquipDefault()
{
	Equip(myWeapon);
}

void ABaseCharacter::CreateHitEffects(float Damage, const FDamageEvent& DamageEvent, APawn* PawnInstigator, AActor* DamageCauser)
{
	if (Role == ROLE_Authority)
		ReplicateHit(Damage, DamageEvent, PawnInstigator, DamageCauser, false);

	//if (Damage > 0.f)
	//	ApplyDamageMomentum(Damage, DamageEvent, PawnInstigator, DamageCauser);

	//TODO make destinction for critical hits
	myOnCharacterWasHit.Broadcast(PawnInstigator->GetActorLocation());
	ABaseCharacter* character = Cast<ABaseCharacter>(PawnInstigator);
	character->myOnCharacterHitTarget.Broadcast(false);
}

void ABaseCharacter::TornOff()
{
	SetLifeSpan(25.f);
}

bool ABaseCharacter::IsEnemyOf(AController* OtherController) const
{
	if (!OtherController || OtherController == Controller)
		return false;

	ABasePlayerState* otherPlayerState = Cast<ABasePlayerState>(OtherController->PlayerState);
	ABasePlayerState* playerState = Cast<ABasePlayerState>(APawn::GetPlayerState());

	bool bIsEnemy = true;
	if (GetWorld()->GetGameState())
	{
		//GetWorld()->GetGameState()->GetDefaultGameMode<ABaseGameMode>();
		const ABaseGameMode* baseGameMode = UFunctionLibrary::GetGameMode(this);
		if (baseGameMode && playerState && otherPlayerState)
		{
			bIsEnemy = baseGameMode->CanDamage(otherPlayerState, playerState);
		}
	}

	return bIsEnemy;
}

ABasePlayerController* ABaseCharacter::GetAssistingPlayer(AController * Killer) const
{
	ABasePlayerController* assist = nullptr;
	float maxDamage = 0.0f;
	for (auto damageDealer : myDamageDealers)
	{
		if (damageDealer.Value > maxDamage && damageDealer.Key != Killer)
		{
			maxDamage = damageDealer.Value;
			assist = damageDealer.Key;
		}
	}

	return maxDamage >= 0.25f * myMaxHealth ? assist : nullptr;
}

bool ABaseCharacter::IsEnemyOf(AActor* OtherActor) const
{
	ABaseCharacter* otherCharacter = Cast<ABaseCharacter>(OtherActor);
	if (!otherCharacter || otherCharacter == this)
		return false;

	return IsEnemyOf(otherCharacter->GetController());
}

void ABaseCharacter::SetRagdollPhysics()
{
	bool bInRagdoll = false;
	USkeletalMeshComponent* mesh = GetCharacterMesh(false);

	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECR_Ignore);

	if (IsPendingKill())
	{
		bInRagdoll = false;
	}
	else if (!mesh || !mesh->GetPhysicsAsset())
	{
		bInRagdoll = false;
	}
	else
	{
		mesh->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);
		RootComponent = mesh;
		mesh->SetAllBodiesSimulatePhysics(true);
		mesh->SetSimulatePhysics(true);
		mesh->WakeAllRigidBodies();
		mesh->bBlendPhysics = true;

		FDamageEvent const& damageEvent = myLastTakenHitInfo.GetDamageEvent();
		if (damageEvent.IsOfType(1))
		{
			const FPointDamageEvent* pointDamageEvent = static_cast<const FPointDamageEvent*>(&damageEvent);
			mesh->AddImpulseAtLocation(pointDamageEvent->ShotDirection * pointDamageEvent->Damage, pointDamageEvent->HitInfo.Location, pointDamageEvent->HitInfo.BoneName);
		}
		else if (damageEvent.IsOfType(2))
		{
			const FRadialDamageEvent* radialDamageEvent = static_cast<const FRadialDamageEvent*>(&damageEvent);
			mesh->AddRadialImpulse(radialDamageEvent->Origin, 100.0f, radialDamageEvent->Params.BaseDamage, ERadialImpulseFalloff::RIF_Linear);
		}

		bUseControllerRotationPitch = false;
		bUseControllerRotationYaw = false;
		bUseControllerRotationRoll = false;
		bInRagdoll = true;
	}

	GetCharacterMovement()->StopMovementImmediately();
	GetCharacterMovement()->DisableMovement();
	GetCharacterMovement()->SetComponentTickEnabled(false);

	if (!bInRagdoll)
	{
		// hide and set short lifespan
		TurnOff();
		SetActorHiddenInGame(true);
		SetLifeSpan(1.0f);
	}
	else
	{
		SetLifeSpan(10.0f);
	}
}

bool ABaseCharacter::IsLocalClientOwned() const
{
	if (auto pc = Cast<ABasePlayerController>(GetController()))
		return pc->IsLocalController();
	return false;
}

void ABaseCharacter::StartFire()
{
	ABasePlayerController* controller = Cast<ABasePlayerController>(Controller);
	if (!controller || !controller->IsGameInputAllowed())
		return;

	// TODO this will need tweaking for high latency situations (client decision only for now)
	if (myWeapon)
	{
		FHitResult OutHit;
		FVector start = myCamera->GetComponentLocation();
		FVector end = start + myCamera->GetForwardVector() * 100000.0f;
		FCollisionQueryParams CollisionParams;
		CollisionParams.AddIgnoredActor(this);
		CollisionParams.AddIgnoredActor(myWeapon);
		FCollisionResponseParams CollisionResponseParams;
		if (GetWorld()->SweepSingleByChannel(OutHit, start, end, FQuat::Identity, ECollisionChannel::ECC_Visibility, FCollisionShape::MakeSphere(5.0f), CollisionParams, CollisionResponseParams))
			myWeapon->Fire(OutHit.Location);
		else
			myWeapon->Fire(end);
	}
}

void ABaseCharacter::StopFire()
{
}

void ABaseCharacter::MoveForward(float Amount)
{
	ABasePlayerController* controller = Cast<ABasePlayerController>(Controller);
	if (!controller || !controller->IsGameInputAllowed())
		return;

	if (Amount == 0.0f)
		return;

	AddMovementInput(GetActorForwardVector(), Amount);
}

void ABaseCharacter::MoveRight(float Amount)
{
	ABasePlayerController* controller = Cast<ABasePlayerController>(Controller);
	if (!controller || !controller->IsGameInputAllowed())
		return;

	if (Amount == 0.0f)
		return;

	AddMovementInput(GetActorRightVector(), Amount);
}

void ABaseCharacter::Turn(float Amount)
{
	ABasePlayerController* controller = Cast<ABasePlayerController>(Controller);
	if (!controller || !controller->IsGameInputAllowed())
		return;

	if (Amount != 0.0f)
	{
		AddControllerYawInput(Amount * myTurnRate * GetWorld()->GetDeltaSeconds());
	}
}

void ABaseCharacter::LookUp(float Amount)
{
	ABasePlayerController* controller = Cast<ABasePlayerController>(Controller);
	if (!controller || !controller->IsGameInputAllowed())
		return;

	if (Amount != 0.0f)
	{
		AddControllerPitchInput(Amount * myLookUpRate * GetWorld()->GetDeltaSeconds());
	}
}

void ABaseCharacter::SetCrouch(bool IsCrouching)
{
	myWantsToCrouch = IsCrouching;
	if(IsCrouching)
		UpdateCharacterState(ECharacterStateFlag::Crouch);
	else
		UpdateCharacterState(ECharacterStateFlag::None);

	if (!HasAuthority())
		Server_SetCharacterStateFlag(ECharacterStateFlag::Crouch, IsCrouching);
}

void ABaseCharacter::StartCrouch()
{
	ABasePlayerController* controller = Cast<ABasePlayerController>(Controller);
	if (controller && controller->IsGameInputAllowed())
	{
		SetCrouch(true);
	}
}

void ABaseCharacter::ToggleCrouch()
{
	ABasePlayerController* controller = Cast<ABasePlayerController>(Controller);
	if (controller && controller->IsGameInputAllowed())
	{
		SetCrouch(!myWantsToCrouch);
	}
}

void ABaseCharacter::StopCrouch()
{
	SetCrouch(false);
}

void ABaseCharacter::SetSprint(bool IsSprinting)
{
	myWantsToSprint = IsSprinting;
	if(IsSprinting)
		UpdateCharacterState(ECharacterStateFlag::Sprint);
	else
		UpdateCharacterState(ECharacterStateFlag::None);

	if (!HasAuthority())
		Server_SetCharacterStateFlag(ECharacterStateFlag::Sprint, IsSprinting);
}

void ABaseCharacter::StartSprint()
{
	ABasePlayerController* controller = Cast<ABasePlayerController>(Controller);
	if (controller && controller->IsGameInputAllowed())
	{
		SetSprint(true);
	}
}

void ABaseCharacter::ToggleSprint()
{
	ABasePlayerController* controller = Cast<ABasePlayerController>(Controller);
	if (controller && controller->IsGameInputAllowed())
	{
		SetSprint(!myWantsToSprint);
	}
}

void ABaseCharacter::StopSprint()
{
	SetSprint(false);
}

void ABaseCharacter::StartJump()
{
	ABasePlayerController* controller = Cast<ABasePlayerController>(Controller);
	if (controller && controller->IsGameInputAllowed())
	{
		bPressedJump = true;
	}
}

void ABaseCharacter::StopJump()
{
	bPressedJump = false;
	StopJumping();
}

void ABaseCharacter::Reload()
{
	ABasePlayerController* controller = Cast<ABasePlayerController>(Controller);
	if (!controller || !controller->IsGameInputAllowed())
		return;

	if (myWeapon->CanReload())
		myWeapon->StartReload();
}

void ABaseCharacter::ActivateAbility()
{
	ABasePlayerController* controller = Cast<ABasePlayerController>(Controller);
	if (!controller || !controller->IsGameInputAllowed())
		return;

	if (myAbilities.IsValidIndex(0))
		myAbilities[0]->TriggerAbility();
}

void ABaseCharacter::Client_PossessedBy_Implementation(ABasePlayerController * PlayerController)
{
	check(IsLocallyControlled());

	CreateIngameUI();
	OnPossession();
}

void ABaseCharacter::SetWalk(bool IsWalking)
{
	myWantsToWalk = IsWalking;
	if(IsWalking)
		UpdateCharacterState(ECharacterStateFlag::Walk);
	else
		UpdateCharacterState(ECharacterStateFlag::None);

	if (!HasAuthority())
		Server_SetCharacterStateFlag(ECharacterStateFlag::Walk, IsWalking);
}

void ABaseCharacter::StartWalk()
{
	ABasePlayerController* controller = Cast<ABasePlayerController>(Controller);
	if (controller && controller->IsGameInputAllowed())
	{
		SetWalk(true);
	}
}

void ABaseCharacter::ToggleWalk()
{
	ABasePlayerController* controller = Cast<ABasePlayerController>(Controller);
	if (controller && controller->IsGameInputAllowed())
	{
		SetWalk(!myWantsToWalk);
	}
}

void ABaseCharacter::StopWalk()
{
	SetWalk(false);
}

void ABaseCharacter::SetAim(bool IsAiming)
{
	myWantsToAim = IsAiming;
	if (IsAiming)
		UpdateCharacterState(ECharacterStateFlag::Aim);
	else
		UpdateCharacterState(ECharacterStateFlag::None);

	if (!HasAuthority())
		Server_SetCharacterStateFlag(ECharacterStateFlag::Aim, IsAiming);
}

void ABaseCharacter::StartAim()
{
	ABasePlayerController* controller = Cast<ABasePlayerController>(Controller);
	if (controller && controller->IsGameInputAllowed())
	{
		SetAim(true);
	}
}

void ABaseCharacter::ToggleAim()
{
	ABasePlayerController* controller = Cast<ABasePlayerController>(Controller);
	if (controller && controller->IsGameInputAllowed())
	{
		SetAim(!myWantsToAim);
	}
}

void ABaseCharacter::StopAim()
{
	SetAim(false);
}

void ABaseCharacter::UpdateCharacterState(ECharacterStateFlag LatestInput)
{
	switch (LatestInput)
	{
	case ECharacterStateFlag::Aim:
		myIsAiming = true;
		myIsSprinting = false;
		break;
	case ECharacterStateFlag::Crouch:
		myIsCrouching = true;
		myIsSprinting = false;
		myIsWalking = false;
		break;
	case ECharacterStateFlag::Sprint:
		myIsSprinting = true;
		myIsAiming = false;
		myIsWalking = false;
		myIsCrouching = false;
		break;
	case ECharacterStateFlag::Walk:
		myIsWalking = true;
		myIsSprinting = false;
		myIsCrouching = false;
		break;
	case ECharacterStateFlag::None:
	default:
		SetHighestPriorityState();
	}
}

void ABaseCharacter::SetHighestPriorityState()
{
	myIsAiming = false;
	myIsSprinting = false;
	myIsWalking = false;
	myIsCrouching = false;

	if (myWantsToSprint)
	{
		myIsSprinting = true;
		return;
	}
	if (myWantsToAim)
		myIsAiming = true;
	if (myWantsToWalk)
		myIsWalking = true;
	else if (myWantsToCrouch)
		myIsCrouching = true;
}

bool ABaseCharacter::IsAiming() const
{
	return myIsAiming;
}

bool ABaseCharacter::IsCrouching() const
{
	return myIsCrouching;
}

bool ABaseCharacter::IsSprinting() const
{
	return myIsSprinting && !GetVelocity().IsZero() && (GetVelocity().GetSafeNormal2D() | GetActorForwardVector()) > -0.1;
}

bool ABaseCharacter::IsWalking() const
{
	return myIsWalking;
}

bool ABaseCharacter::IsFirstPerson() const
{
	return IsAlive() && Controller && Controller->IsLocalPlayerController();
}

bool ABaseCharacter::IsUsingEnergy() const
{
	for (UAbilityComponent* ability : myAbilities)
	{
		if (ability->IsAbilityActive())
			return true;
	}
	return false;
}

bool ABaseCharacter::UseEnergy(float Amount)
{
	if(myEnergy < Amount)
		return false;

	myEnergy -= Amount;
	myEnergyRegenerationTimer = myEnergyRegenerationCooldown;
	return true;
}

float ABaseCharacter::GetEnergy() const
{
	return myEnergy;
}

void ABaseCharacter::AddAbility(UAbilityComponent * Ability)
{
	if(Ability)
		myAbilities.Add(Ability);
}

void ABaseCharacter::Server_SetCharacterStateFlag_Implementation(ECharacterStateFlag Flag, bool State)
{
	check(HasAuthority());

	switch (Flag)
	{
	case ECharacterStateFlag::Aim:
		SetAim(State);
		break;
	case ECharacterStateFlag::Crouch:
		SetCrouch(State);
		break;
	case ECharacterStateFlag::Walk:
		SetWalk(State);
		break;
	case ECharacterStateFlag::Sprint:
		SetSprint(State);
		break;
	default:
		break;
	}
}

bool ABaseCharacter::Server_SetCharacterStateFlag_Validate(ECharacterStateFlag Flag, bool State)
{
	return true;
}